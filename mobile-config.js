// configure app's icons, title, version number, splash screen, and other metadata
App.info({
  name: 'Werewolf',
  description: 'The Moderator App for Werewolf Games',
  author: 'Vivian Hou',
  email: 'vivian.w.hou@gmail.com',
  website: 'http://percolatestudio.com',
  version: '0.0.1'
});

// For html5 media 
App.setPreference('AllowInlineMediaPlayback', true);