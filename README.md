=======
Usage (Temporary):

0) This is (for now) only catered to in-person games. Sitting close to whoever is playing with you is necessary and presumed.
1) Sign in

2) Create a new game, or join a existing game by entering the name (6-character string)

3) In the game waiting room, a list of players in the room is shown. A random player is assigned as the owner, and he/she will be able to launch the game when everyone is ready. Any player is allowed to leave the room.

4) Once the game has been launched: 
  
 4a) Game is locked and anyone other than current players won't be able to join. But if someone inside got disconnected somehow, they can come back.
  
 4b) Game owner choose type/number of roles (on behalf of others). The total of number of roles must match the number of players before game can be started. (There should be some other prerequisites, and will be added later.) 

 4c) On start of game, each player will be randomly assigned to a role and asked to confirm that he/she understands the rules and is ready. After everyone is ready, the game will enter the night/day cycle until one party wins. 

 4d) (Yet to come:) In the night cycle, different roles will be waken to play their duty, killing or saving or other, each part is timed and the day comes after. In the day cycle, everyone is waken to see the outcome, discuss and vote. This part is not timed, but needs player actions to continue to night.

 4e) (Yet to come:) When game is finished, the stats will be shown. There should also be a timer for each game, that if somehow it doesn't finished in a reasonable amount of time (possibly abandoned), it will be marked as aborted and no longer active (maybe recycled).

 4f) (Yet to come:) Players would be able to view history game stats on their dashboard.
