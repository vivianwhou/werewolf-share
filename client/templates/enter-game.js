Template.enterGame.events({

  "click .search-game": function () {
    bootbox.prompt({
      title: "Enter game name",
      callback: function (gameName) {
        if (gameName === "" || gameName === null) {
          bootbox.alert("Please enter a valid game name");  
       	} else {
          var currentUser = Meteor.user();
          Meteor.call("searchGame", gameName, function (err, gameId) {
            if (gameId) {
              Meteor.call("joinGame", gameId, currentUser, function (err, success) {
                if ( success ) 
                  Router.go("controlRoom", {_id: gameId}); 
                else {
                	if (err.error == "in_progress")
                		bootbox.alert("Game already in progress. Try another one.");
                }
              });                          
            } else {
            	if (err.error == "not_found") {
            		bootbox.alert("Game does not exist. Try another one."); 
            	}     
            	// else (to handle other exceptions) TO_DO             
            }
          })         
        }
      }
    }) 
  },

  "click .create-game": function () {    
    var currentUser = Meteor.user();
    Meteor.call("createGame", currentUser, function(e, gameId) {
      Router.go("controlRoom", {_id: gameId});
    }); 
  }
});
