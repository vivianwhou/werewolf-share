/* ----------------------  "gameFlow"  ---------------------- */

Template.gameFlow.helpers({
	isOwner: function () {
		return this.owner._id === Meteor.userId();
	},

	gameStarted: function () {
		return this.status === GAME_STATUS_STARTED;
	},

	everyoneIsReady: function () {
		var self = this;
		return self.isready.every(function (each) {
			if (!each.ready) 
				return false;
			return true;
		});		
	},

	gameAborted: function () {
		return this.status === GAME_STATUS_ABORTED;
	},

	gameFinished: function () {
		return this.status === GAME_STATUS_FINISHED;
  }
});

Template.gameFlow.events({
	"click .back-to-waiting-room-owner": function (e) {
		e.preventDefault();
		
    bootbox.confirm("Really want to cancel the game?", function (goCancel) {
      if (goCancel) { 
        var gameId = e.currentTarget.name;
        Meteor.call("cancelGame", gameId, Meteor.userId());               
      }           
    });       
  }	
});

/* ----------------------  "gameFlow"  ---------------------- */



/* ----------------------  "showRoleAssignment"  ---------------------- */

Template.showRoleAssignment.onCreated(function () {
	var self = this;
	var gameId = self.data._id;
	Meteor.call("getRole", Meteor.userId(), gameId, function (err, role) {
		if (err)
			console.log(err);
		else
			Session.set("YOUR_ROLE", role);
	});
})

Template.showRoleAssignment.helpers({
  currentPlayerRole: function () {
  	return Session.get("YOUR_ROLE");
  }
});

Template.showRoleAssignment.events({
"click .ready-to-game": function (e) {
		e.preventDefault();

		var self = this;
		var gameId = self._id;
		bootbox.confirm("Ready?", function (ready) {
			if (ready) {
				Meteor.call("flagPlayerReady", gameId, Meteor.userId(), function (err, result) {
					if ( err ) 
						console.log("something wrong  ", err);
				});
			}
		});
	}
});

/* ----------------------  "showRoleAssignment"  ---------------------- */