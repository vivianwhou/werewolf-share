Template.waitingRoom.helpers({
  count: function () {
    return this.players.length;   
  },   

  players: function () {
    return this.players;             
  },

  isOwner: function () {
    return this.owner._id === Meteor.userId();
  },
  
  isSelecting: function () {
    return this.status === GAME_STATUS_ROLE_SELECTION;
  }
});

Template.waitingRoom.events({
  "click .goto-role-selection-owner": function (e) {
    // only seen by owner of game
    e.preventDefault();
    bootbox.confirm("Ready to start the game?", function (isReady) {
      if (isReady) { 
        var gameId = e.currentTarget.name;
        Meteor.call("toAssignRoles", gameId);
      }
    });        
  },

  "click .back-to-main-room": function (e) {
    e.preventDefault();
    bootbox.confirm("Really want to leave this game?", function (leaveGame) {
      if (leaveGame) {
        var currentPlayer = Meteor.user();
        var gameId = e.currentTarget.name;                 

        Meteor.call("checkOut", gameId, currentPlayer, function (err, checkedOut) {
          if (checkedOut) {
            Router.go("enterGame");
          }
        });
      }
    });
  }
});
