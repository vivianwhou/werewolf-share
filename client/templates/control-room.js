Template.controlRoom.helpers({
	isWaiting: function () {
		return this.status === GAME_STATUS_WAITING;
	},

	isSelecting: function () {
		return this.status === GAME_STATUS_ROLE_SELECTION;
	},

	hasStarted: function () {
		return this.status === GAME_STATUS_STARTED;
	}
});