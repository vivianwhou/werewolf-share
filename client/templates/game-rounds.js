var TTSURL = "http://translate.google.com/translate_tts?tl=en&";

/* ------- Game flow to be added here ------ */
// TO_BE_DONE
//
/* ------- Game flow to be added here ------ */


/* ----------------------  "gameRounds"  ---------------------- */

Template.gameRounds.helpers({
	isNight: function () {
		return this.timeofday === "night";
	},

	isDay: function () {
		return this.timeofday === "day";
	}
});
/* ----------------------  "gameRounds"  ---------------------- */


/* ----------------------  "nightRound"  ---------------------- */

Template.nightRound.helpers({

	// Google TTS requests often fails. Will use another solution.

	nightBeginAudio: function () {
		var query = "q=The+night+has+begun+.+Everyone+close+your+eyes";
		return TTSURL + query;
	},

	wakeWerewolvesAudio: function () {
		// "kill" is NOT allowed in google translator tts?
		// var query = "q=Werewolves+,+open+your+eyes+and+kill";      
		var query = "q=Werewolves+,+open+your+eyes";
		return TTSURL + query;
	}
});

Template.nightRound.events({
	// temporary: testing audio 
	"click .test": function (e) {
		e.preventDefault();
		var audio = new Audio("");
		audio.play();
	} 
});
/* ----------------------  "nightRound"  ---------------------- */



/* ----------------------  "dayRound"  ---------------------- */

Template.dayRound.helpers({
	dayBeginAudio: function () {
		var query = "q=The+day+has+begun+.+Everyone+,+open+your+eyes";
		return TTSURL + query;
	}
})
/* ----------------------  "dayRound"  ---------------------- */