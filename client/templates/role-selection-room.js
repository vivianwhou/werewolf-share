var showDescription = function (description) {
	bootbox.dialog({
    message: description,
    title: "Role description",
    buttons: { 
      main: {
          label: "OK",
          className: "btn-primary",
          callback: true
      }
    }
  });
}

/* ----------------------  "roleSelection"  ---------------------- */

Template.roleSelection.helpers({
  count: function () {
    return this.players.length;      
  },

  players: function () {
    return this.players;          
  },

  isOwner: function () {
    return this.owner._id === Meteor.userId();
  },

  gameOff: function () {
    return this.status === GAME_STATUS_WAITING;
  },

  unassignedPlayers: function () {
    var self = this;
    var assigned = 0;
    this.roles.forEach(function (role) {
    	assigned += role.count;
    })
    Session.set("UNASSIGNED", this.players.length - assigned)
    return Session.get("UNASSIGNED");
  },

  roles: function () {                             
    return this.roles;
  }
});

Template.roleSelection.events({
  "click .start-game": function (e) {
    // only seen by owner of game
    e.preventDefault();
    var gameId = e.currentTarget.name;
    var unassigned = Session.get("UNASSIGNED"); 
    if ( unassigned === 0 ) {
      bootbox.confirm("Confirm to start game.", function (confirmed) {
        if (confirmed) {
          Meteor.call("initGame", gameId, Meteor.userId(), function (err) {
          	if (err)
          		console.log(err);
          });
        }
      })
    } else {
    	bootbox.alert( unassigned + " players unassigned.");
    }
  },

  "click .back-to-waiting-room-owner": function (e) {
    e.preventDefault();
    bootbox.confirm("Really want to cancel the game?", function (goCancel) {
      if (goCancel) {               
        var gameId = e.currentTarget.name;
        Meteor.call("cancelGame", gameId, Meteor.userId(), function (err) {
        	if (err) 
        		console.log(err);
        });               
      }           
    });       
  }
});

Template.roleOwner.events({
  "click .show-description": function (e) {
    e.preventDefault();  
    showDescription(this.description);
  },

  "click .increase-by-one": function (e) {                           
    e.preventDefault();
    var gameId = e.currentTarget.name;

    var self = this;
    Meteor.call("updateRoleCount", gameId, self.name, 1);      
  },

  "click .decrease-by-one": function (e) {
    e.preventDefault();
    var gameId = e.currentTarget.name;
    
    var self = this;
    if (self.count >= 1) {
      Meteor.call("updateRoleCount", gameId, self.name, -1);  
    }
  }
});

Template.role.events({
  "click .show-description": function (e) {
    e.preventDefault();
    showDescription(this.description);
  },
});

/* ----------------------  "roleSelection"  ---------------------- */