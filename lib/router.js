Router.configure({
  layoutTemplate: "mainLayout",
  loadingTemplate: "loading"
});

Router.map(function() {
  this.route("login", {
    path: "/",
    onBeforeAction: function() {
      if (Meteor.user()) {
        Router.go("enterGame");
      }
      this.next();
    }
  });

  this.route('enterGame', {
    path: '/game',
    layoutTemplate: 'enterGame',    
    waitOn: function() {
      // [need to be removed] subscriptions only for test 
      return [Meteor.subscribe('allGames'), 
              Meteor.subscribe('roles')];
    }
  });

  this.route('controlRoom', {
    path: '/game/:_id',
    layoutTemplate: 'controlRoom',
    waitOn: function() {
      return Meteor.subscribe('singleGame', this.params._id);
    },
    onBeforeAction: function() {
      if ( ! GameCollection.findOne(this.params._id) ) {
        Router.go('enterGame');
      } else { this.next() }
    },
    data: function () {
      return GameCollection.findOne(this.params._id);
    }
  });
});

/* -------  User must be logged in to continue -------- */
var mustBeSignedIn = function() {
  if (!(Meteor.user()) && !(Meteor.loggingIn())) {
    Router.go('login');
  }
  this.next();                     
};
Router.onBeforeAction(mustBeSignedIn, {except: ['login']});
