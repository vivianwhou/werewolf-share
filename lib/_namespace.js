// Global variables (constants) 
// const is not supported by all browsers

// ? declare a single gloable variable MyApp = {} 

GAME_STATUS_WAITING = "waiting";                   

GAME_STATUS_ROLE_SELECTION = "role selecting";

GAME_STATUS_STARTED = "started";

GAME_STATUS_ABORTED = "aborted";

GAME_STATUS_FINISHED = "finished";