GameCollection = new Mongo.Collection("games");
RoleCollection = new Mongo.Collection("roles");

// Modifications is not allowed from the client, 
// but can only be done with methods.
GameCollection.allow({
  insert: function () {                     
    return false;
  },
  update: function () {                     
    return false;
  },
  remove: function () {                     
    return false;
  }
});

/*  -----  will be added later to create user dashboard ------ */
// PlayerCollection = new Mongo.Collection('players');
