// if the database is empty on server start, create some sample data.
// For more information about the roles, please visit
//     http://www.playwerewolf.co/rules/roles/
Meteor.startup(function () {
  if (RoleCollection.find().count() === 0) {
    var data = [
      { name: "Villagers", description: "Villagers"},
      { name: "Werewolves", description: "Werewolves"},
      { name: "Seer", description: "Seer"},
      { name: "Doctor", description: "Doctor"},
      { name: "Village Drunk", description: "Village Drunk"},
      { name: "Witch", description: "Witch"},
      { name: "Alpha Werewolf", description: "Werewolves"}
    ];
    _.each(data, function(role) {
      var role_id = RoleCollection.insert({
        name: role.name,
        description: role.description,
        count: 0
      })
    });
  }
}); 

