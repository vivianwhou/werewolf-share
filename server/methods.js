// Verify user is in game to make any changes to the game
function verifyUserInGame (gameId, userId) {
	var found = GameCollection.findOne({_id: gameId, 
		"players._id": userId});
	return found;
}

/*------------------------------- methods ---------------------------------------*/
Meteor.methods({
  searchGame: function (gameName) {
    check(gameName, String);
    var gameFound = GameCollection.findOne({name: gameName});
    if (gameFound) {
    	return gameFound._id
    } else {
      throw new Meteor.Error("not_found", "Game doesn't exist.");
    }       
  },

  // returns a boolean indicating whether current player
  // is allowed to join the game
  // planning to add more exception handling
  joinGame: function (gameId, currentPlayer) {
    check(gameId, String);
    var gameFound = GameCollection.findOne(gameId);
    var userInGame = verifyUserInGame(gameId, currentPlayer._id)   
    if (userInGame) {
    	return true;
    } else {
    	if ( gameFound.status === GAME_STATUS_WAITING ) {
        GameCollection.update(
          { _id: gameId },
          { $push: { players: currentPlayer } }
        );
        return true;
    	} else {
    		throw new Meteor.Error("in_progress", "Game already in progress");
    	}
    }
  },

  createGame: function (currentPlayer) {
  	var gameName = Random.id(6);
    while (GameCollection.findOne({name:gameName})) {
      gameName = Random.id(6);
    }
    var basicRoles = RoleCollection.find().fetch();
    var schema = {
      name: gameName,
      created: new Date(),
      owner: currentPlayer,
      players: [currentPlayer],             // _.extend({role: undefined}) 
      roles: basicRoles,                    // _.extend({count, players, alive})
      status: GAME_STATUS_WAITING,          // 'role-selecting', 'started', 'active', 'finished', 'closed'
    };
    var gameId = GameCollection.insert(schema);
    return gameId;
  },

  // check current player out of the game waiting room
  // if he/she is the last one and game isn't finished, delete the game
  checkOut: function (gameId, currentPlayer) {
  	check(gameId, String);
    var game = GameCollection.findOne(gameId);
    if (game.players.length === 1 && game.status !== GAME_STATUS_FINISHED) {
      GameCollection.remove({_id: gameId});
    } else { 
      GameCollection.update(
        { _id: gameId, "players._id": currentPlayer._id },
        { $pull: { players: { _id: currentPlayer._id } } }
      );      

      if (currentPlayer._id === game.owner._id) {
        var newOwner = GameCollection.findOne(gameId).players[0];
        GameCollection.update(
          { _id: gameId },
          { $set: { owner: newOwner } }  
        );
      }
      return true;
    }
  },

  // change the number of each role 
  updateRoleCount: function (gameId, roleName, num) {
  	check(gameId, String);
  	check(roleName, String);
  	check(num, Number);
    var pipeline = [ 
      { $match: { _id: gameId } },   
      { $unwind: "$roles" },  
      { $group: { _id: "$_id", nRoles: { $sum: "$roles.count"} } } 
    ];
    var currRoles = GameCollection.aggregate(pipeline)[0].nRoles;

    var minRoles = 0;
    var maxRoles = GameCollection.findOne(gameId).players.length;
    if ((num + currRoles <= maxRoles) && (num + currRoles >= minRoles)) {
      GameCollection.update(
        { _id: gameId, "roles.name": roleName },   
        { $inc: { "roles.$.count": num } } 
      );
    }
  },

  // currently not in use
  totalRoles: function (gameId) {
  	check(gameId, String);
    var pipeline = [ 
      { $match: { _id: gameId } },   
      { $unwind: "$roles" },  
      { $group: { _id: "$_id", nRoles: { $sum: "$roles.count"} } } 
    ];
    return GameCollection.aggregate(pipeline)[0].nRoles;
  },

  // game owner only
  cancelGame: function (gameId, playerId) {
  	check(gameId, String);
    GameCollection.update(
      { _id: gameId, "owner._id": playerId },
      { $set: { status: GAME_STATUS_WAITING } },
      { $unset:	{ isready: 1 } }     
    );
  },

  toAssignRoles: function (gameId) {
  	check(gameId, String);
    GameCollection.update(
      { _id: gameId },
      { $set: { status: GAME_STATUS_ROLE_SELECTION } }
    );
  },

  // make random role assignment to players
  // and initialize the game
  initGame: function (gameId, playerId) {
  	check(gameId, String);
    var playerList = GameCollection.findOne({_id:gameId, 
    	"owner._id": playerId}).players;

    var pipeline = [ 
      { $match: { _id: gameId, "owner._id": playerId } },   
      { $unwind: "$roles" },  
      { $group: { _id: "$_id", 
                  roles: { $push: {name: "$roles.name"} } } } 
    ];
    var roleList = GameCollection.aggregate(pipeline)[0].roles;

    var shuffle = function (array) {
      var currentIndex = array.length, temporaryValue, randomIndex ;
      while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }
      return array;
    }

    shuffle(playerList);
    var playerReady = [];
    for (var i = 0; i < playerList.length; i++) {
      playerList[i].role = roleList[i].name;
      playerList[i].alive = true;
      playerReady.push({_id: playerList[i]._id, 
      									username: playerList[i].username,
      									ready: false});
    }

    GameCollection.update(
      { _id: gameId },
      { $set: { 
        status: GAME_STATUS_STARTED,
        started: new Date(),
        round: 1,
        timeofday: "night",
        players: playerList, 
      	isready: playerReady } } 
		);
  },

  // get role assignment result for current player
  getRole: function (playerId, gameId) {
  	check(playerId, String);
  	check(gameId, String);
    var pipeline = [ 
      { $match: { _id: gameId, "players._id": playerId} },   
      { $unwind: "$players" },  
      { $project: { _id: "$players._id", 
                    role: "$players.role", } } 
    ];
    var players = GameCollection.aggregate(pipeline);

    for (var i = 0; i < players.length; i++) {
      if (players[i]._id === playerId) {
        return players[i].role;
      }
    };
  },

  flagPlayerReady: function (gameId, playerId) {
  	check(playerId, String);
  	check(gameId, String);
  	GameCollection.update(
        { _id: gameId, "isready._id": playerId },   
        { $set: { "isready.$.ready": true } } 
    );
  },

  // currently not in use
  everyoneReady: function (gameId) {
  	check(gameId, String);
  	var pipeline = [
			{ $match: { _id: gameId } },
 			{ $unwind: "$isready" },
 			{ $group: { _id: null, 
 									ready: { $push: "$isready.ready" } } }
		]
		var readyState = GameCollection.aggregate(pipeline)[0].ready;
		return readyState.every(function (each) {
			if (!each) {
				return false;
			}
			return true;
		});
	}
});
