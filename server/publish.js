/* ----------------------------------- games ----------------------------------- */
Meteor.publish("allGames", function () {
  return GameCollection.find({});              
});

Meteor.publish("singleGame", function (gameId) {

  check(gameId, String);
  return GameCollection.find( { _id:gameId }, 
                              { fields: 
                                { "players.role": 0 } } );
});

Meteor.publish("roles", function () {
  return RoleCollection.find({});
});
